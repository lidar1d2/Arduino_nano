/* Distance measurments
  by Arnaud Joset <www.agayon.be>
  This example code is released under the GPLV3 licence.
  This code is inspired by the work of:
  - Sweep by BARRAGAN <http://barraganstudio.com>
  - LIDARLite Arduino Library GetDistancePwm (Garmin)

  modified 30 March 2017
  by Arnaud Joset
*/


/*
   PINS
   1 :
   2 : Encoder disk: Interrupt
   3 : Monitor pin : yellow cable (no resistance)
   4 : Trigger Pin : white  cable (1K Ohm  resistance)
   5
   6 :
   7 :
   8 :
   9 : Servo



*/


#include <Wire.h>
#include <Servo.h>

unsigned const int threshold = 150;
int incomingByte;   // for incoming serial data
// number of acquisition
const int data_size = 360;
//int distances[data_size];
//int deltas[data_size];


unsigned int n_acquisition = data_size;

#define disk_pin 2
volatile unsigned int global_count = 0; // universal count
volatile unsigned int local_count = 0;
unsigned int global_count_flag = 0;
unsigned int local_count_flag = 0;

volatile byte flag = 0; // interrupt status flag
int disk_step_nbr = 360;

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int mini = 0;// minimum position Servo
int maxi = 180; // Maximum position Servo
int pos = 90; // variable to store the servo position

boolean Servo_state = 1;

unsigned long pulseWidthDist;
unsigned long currentMillis, previousServoMillis, previousLidarMillis, previousDiskMillis, previousRotationMillis;
unsigned int intervalServo = 10, intervalLidar = 1 ;
int monitorPin = 3;
int triggerPin = 4;


void setup() {
  Wire.begin(42);
  Wire.onReceive(MegaRotation); // register event

  Serial.begin(230400); // Start serial communications
  pinMode(disk_pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(disk_pin), disk_flag, RISING);

  pinMode(triggerPin, OUTPUT); // Set pin 4 as trigger pin
  digitalWrite(triggerPin, LOW); // Set trigger LOW for continuous read

  pinMode(monitorPin, INPUT); // Set pin 3 as monitor pin
  Serial.println("Start");


  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  // pin 9 et 10 pour servo shield
  myservo.write(maxi - 5);

  /*
     TODO: routine etalonnage nombre de steps dans le disque
     Lorsque le disque sera réimprimé, on aura plus besoin

  */

}

void loop() {




  /*  Serial.print(Servo_state);
    Serial.print(" ");
    Serial.print(intervalServo);
    Serial.print(" ");
    Serial.println(currentMillis - previousServoMillis);
  */


  // IF 3D with SERVO
  /*
      if (currentMillis - previousServoMillis > intervalServo ) {
        previousServoMillis = currentMillis;
        Servo_state = setServo(Servo_state);
      }
  */
  if (flag)   {

    global_count_flag = global_count;
    local_count_flag = local_count;

    currentMillis = millis();
    unsigned int delta = currentMillis - previousDiskMillis;
    previousDiskMillis = currentMillis;
    /*
       TODO:
       modifier profondément le programme

       Nombre de mesure qu'on veut prendre à fixer
       mesurer/afficher les delta pour voir l'allure des données

       faire des toours au début (dans setup) pour calibrer la roue, s'arrêter à encoche zero pour voir si on arrive à retrouver le zéro

       exemple: 600 points, 4 mesures
       compteur incrémente
       si 4 mesures:
       après compteur = 600/4 = 150
       si compteur > 150, on prend mesure
       On réinitialise compteur local, on comptinue avec le principal
       idem à 300 et 450
       Si delta > max, on a fait un tour (voir pour delta), compteur global = 0, initial aussi, prendre mesure
    */

    if (local_count > float(disk_step_nbr / n_acquisition)) {
      Lidar_dist();
      local_count_flag = 0;
      print_data(delta);
    }

    if (delta > threshold) {
      // Erery rotation, the stepper stop for 1 second. The EOR label is send and counters are reset
      process_data(delta,  true);
      reset_counters();
    }
    //print_data(delta);
    // */
    // end comment for production
    flag = 0; // clear flag
  }
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    Serial.print("RESET count ");
    Serial.print(global_count);
    Serial.println(" ");
    reset_counters();
  }


}






