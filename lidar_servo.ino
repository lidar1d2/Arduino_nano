void Lidar_dist() {

  if (currentMillis - previousLidarMillis > intervalLidar ) {
    previousLidarMillis = currentMillis;
    pulseWidthDist = pulseIn(monitorPin, HIGH); // Count how long the pulse is high in microseconds
    //pulseWidthDist = 3;
    //Serial.print("pulseWidthDist ");
    //Serial.println(pulseWidthDist);
    // If we get a reading that isn't zero, let's print it
    if (pulseWidthDist != 0)
    {
      pulseWidthDist = pulseWidthDist / 10; // 10usec = 1 cm of distance
      //Serial.print("Distance : ");
      //Serial.print(pulseWidthDist); // Print the distance
      //Serial.println(" cm"); // Print the distance
    }
  }
}

//////////////////////////////////////////
// ---- SERVO ---
boolean setServo(boolean Servo_state)// balayage servo de mini à maxi
{
  int increment = 1;
  if (Servo_state == 1) {
    // Serial.println("Rising angle");
    pos += increment;
  }
  else
  {
    // Serial.println("Falling angle");
    pos -= increment;
  }
  //Serial.print("Pos = ");
  //Serial.println(pos);
  if (pos <= mini) {
    Servo_state = 1;
    pos = mini;

  }
  if (pos >= maxi) {
    Servo_state = 0;
    pos = maxi;

  }

  myservo.write(pos);
  return Servo_state;

}
//////////////////////////////////////////
// ---- Interrupt ---
void disk_flag() {
  flag = 1;
  // add 1 to count for CW
  if (digitalRead(disk_pin)) {
    global_count += 1 ;
    local_count += 1;

  }
}

//////////////////////////////////////////
// ---- RESET counts---
void reset_counters() {
  global_count = 0;
  local_count = 0;
  global_count_flag = 0;
  local_count_flag = 0;

}


//////////////////////////////////////////
// ---- I2C Mega ---

void MegaRotation(int n_bytes) {

  Serial.print("RESET ROTATION ");
  Serial.println(global_count);
  global_count = 0;
  local_count = 0;

}
